import React, { Component } from 'react';
import { Link, withRouter } from 'react-router-dom';
import { Container, Form, FormGroup, Label, Input, Button } from 'reactstrap';

class CaregiverEdit extends Component {

    emptyCaregiver = {
        name: '',
        birthDate: new Date(),
        gender: '',
        address: ''
    };

    constructor(props) {
        super(props);
        this.state = { caregiver: this.emptyCaregiver };
        this.submitHandler = this.submitHandler.bind(this);
        this.changeHandler = this.changeHandler.bind(this);
    }

    async componentDidMount() {
        const caregiverId = this.props.match.params.id;
        if (caregiverId !== 'new') {
            const caregiver = await (await fetch(`/api/caregivers/${caregiverId}`)).json();
            this.setState({ caregiver: caregiver });
        }
    }

    async submitHandler(event) {
        event.preventDefault();
        const { caregiver } = this.state;
        await fetch('/api/caregivers', {
            method: (caregiver.id) ? "PUT" : "POST",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(caregiver)
        });
        this.props.history.push('/caregivers');
    }

    changeHandler(event) {
        const nam = event.target.name;
        const val = event.target.value;
        let newCaregiver = { ...this.state.caregiver };
        newCaregiver[nam] = val;
        this.setState({ caregiver: newCaregiver });
    }

    render() {
        const { caregiver } = this.state;
        const title = caregiver.id ? 'Edit Caregiver' : 'Add Caregiver';

        return <div>
            <Container>
                <h2>{title}</h2>
                <Form onSubmit={this.submitHandler}>
                    <FormGroup>
                        <Label for="name">Name</Label>
                        <Input type="text" id="name" name="name" value={caregiver.name || ''} onChange={this.changeHandler} />
                    </FormGroup>
                    <FormGroup>
                        <Label for="birth_date">Birth Date</Label>
                        <Input type="date" id="birth_date" name="birthDate" value={caregiver.birthDate || ''} onChange={this.changeHandler} />
                    </FormGroup>
                    <FormGroup>
                        <Label for="gender">Gender</Label>
                        <Input type="text" id="gender" name="gender" value={caregiver.gender || ''} onChange={this.changeHandler}></Input>
                    </FormGroup>
                    <FormGroup>
                        <Label for="address">Address</Label>
                        <Input type="text" id="address" name="address" value={caregiver.address || ''} onChange={this.changeHandler}></Input>
                    </FormGroup>
                    <FormGroup>
                        <Button type="submit" color="primary">Save</Button>{' '}
                        <Button color="secondary" tag={Link} to="/caregivers">Cancel</Button>
                    </FormGroup>
                </Form>
            </Container>
        </div>
    }
}

export default withRouter(CaregiverEdit);