import React, { Component } from 'react';
import { Button, ButtonGroup, Container, Table } from 'reactstrap'
import { Link } from 'react-router-dom';

class PatientList extends Component {

    emptyPatient = {
        id: ''
    };

    constructor(props) {
        super(props);
        this.state = { patients: [], isLoading: true, patientToDelete: this.emptyPatient };
        this.remove = this.remove.bind(this);
    }

    componentDidMount() {
        this.setState({ isLoading: true });

        fetch('/api/patients')
            .then(response => response.json())
            .then(data => this.setState({ patients: data, isLoading: false }));
    }

    async remove(id) {
        let patient = { ...this.state.emptyPatient };
        patient['id'] = id;
        this.setState({ patientToDelete: patient });
        await fetch('/api/patients', {
            method: 'DELETE',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(patient),
        }).then(() => {
            let updatedPatients = [...this.state.patients].filter(i => i.id !== id);
            this.setState({ patients: updatedPatients });
        });
    }

    render() {
        const { patients, isLoading } = this.state;

        if (isLoading) {
            return <p>Loading...</p>;
        }

        const patientList = patients.map(patient => {
            return <tr key={patient.id}>
                <td>{patient.name}</td>
                <td>{patient.birthDate}</td>
                <td>{patient.gender}</td>
                <td>{patient.address}</td>
                <td>
                    <ButtonGroup>
                        <Button size="sm" color="primary" tag={Link} to={"/patients/" + patient.id}>Edit</Button>
                        <Button size="sm" color="danger" onClick={() => this.remove(patient.id)}>Delete</Button>
                        <Button size="sm" color="secondary" tag={Link} to={"/prescriptions/patient/" + patient.id}>View Medication Plan</Button>
                    </ButtonGroup>
                </td>
            </tr>
        });

        return (
            <div>
                <Container fluid>
                    <div className="float-right">
                        <Button color="success" tag={Link} to={"patients/new/"}>Add Patient</Button>
                    </div>
                    <h3>Patients</h3>
                    <Table className="mt-4">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Birth Date</th>
                                <th>Gender</th>
                                <th>Address</th>
                            </tr>
                        </thead>
                        <tbody>
                            {patientList}
                        </tbody>
                    </Table>
                </Container>
            </div>
        );
    }
}

export default PatientList;