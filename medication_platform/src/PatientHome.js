import React, { Component } from 'react';
import { Container, Button, Table } from 'reactstrap';

class PatientHome extends Component {
    constructor(props) {
        super(props);
        const user = this.props.location.state.user;
        this.state = { user: user, prescriptions: [] };
    }

    componentDidMount() {
        const user = this.state.user;
        fetch(`/api/prescriptions/patient/${user.personId}`)
            .then(response => response.json())
            .then(data => this.setState({ prescriptions: data }));
    }

    render() {
        const { user, prescriptions } = this.state;

        const prescriptionList = prescriptions.map(prescription => {
            return <tr key={prescription.id}>
                <td>{prescription.medication.name}</td>
                <td>{prescription.dailyInterval}</td>
                <td>{prescription.startDate}</td>
                <td>{prescription.endDate}</td>
            </tr>
        });

        return <div>
            <h1>Hello {user.username}</h1>
            <div>
                <Container fluid>
                    <h2>Your Medical Plan</h2>
                    <Table className="mt-4">
                        <thead>
                            <tr>
                                <th>Medication Name</th>
                                <th>Daily Interval</th>
                                <th>Start Date</th>
                                <th>End Date</th>
                            </tr>
                        </thead>
                        <tbody>
                            {prescriptionList}
                        </tbody>
                    </Table>
                </Container>
            </div>
        </div>
    }
}

export default PatientHome;