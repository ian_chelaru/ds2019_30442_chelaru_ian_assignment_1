import React, { Component } from 'react';
import { Container, Table } from 'reactstrap';

class CaregiverHome extends Component {
    constructor(props) {
        super(props);
        const user = this.props.location.state.user;
        this.state = { user: user, patients: [] };
    }

    componentDidMount() {
        const user = this.state.user;
        fetch(`/api/patients/caregiver/${user.personId}`)
            .then(response => response.json())
            .then(data => this.setState({ patients: data }));
    }

    render() {
        const { user, patients } = this.state;

        const patientList = patients.map(patient => {
            return <tr key={patient.id}>
                <td>{patient.name}</td>
                <td>{patient.birthDate}</td>
                <td>{patient.gender}</td>
                <td>{patient.address}</td>
            </tr>
        });

        return <div>
            <h1>Hello {user.username}</h1>
            <div>
                <Container fluid>
                    <h2>Your Pacients</h2>
                    <Table className="mt-4">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Birth Date</th>
                                <th>Gender</th>
                                <th>Address</th>
                            </tr>
                        </thead>
                        <tbody>
                            {patientList}
                        </tbody>
                    </Table>
                </Container>
            </div>
        </div>
    }
}

export default CaregiverHome;