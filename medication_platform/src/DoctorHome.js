import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import {  Button } from 'reactstrap'
import './DoctorHome.css'

class DoctorHome extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return <div id="doctor">
            <br></br>
            <Button size="lg" color="warning" tag={Link} to={"/caregivers"} block>Caregivers</Button>
            <br></br>
            <Button size="lg" color="info" tag={Link} to={"/patients"} block>Patients</Button>
            <br></br>
            <Button size="lg" color="danger" tag={Link} to={"/medications"} block>Medications</Button>
        </div>
    }
}

export default DoctorHome;