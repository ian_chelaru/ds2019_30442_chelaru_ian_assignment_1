import React, { Component } from 'react'
import './App.css';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import CaregiverList from './CaregiverList';
import CaregiverEdit from './CaregiverEdit';
import PatientList from './PatientList';
import PatientEdit from './PatientEdit';
import MedicationList from './MedicationList';
import MedicationEdit from './MedicationEdit';
import DoctorHome from './DoctorHome.js';
import CaregiverHome from './CaregiverHome.js';
import PatientHome from './PatientHome.js';
import Login from './Login.js';
import MedicationPlanEdit from './MedicationPlanEdit.js';
import AddPrescription from './AddPrescription';

class App extends Component {
  render() {
    return (
      <Router>
        <Switch>
          <Route path="/" component={Login} exact={true} />
          <Route path='/caregivers' exact={true} component={CaregiverList} />
          <Route path='/caregivers/:id' component={CaregiverEdit} />
          <Route path='/patients' exact={true} component={PatientList} />
          <Route path='/patients/:id' component={PatientEdit} />
          <Route path='/medications' exact={true} component={MedicationList} />
          <Route path='/medications/:id' component={MedicationEdit} />
          <Route path='/doctor' component={DoctorHome} />
          <Route path='/caregiver' component={CaregiverHome} />
          <Route path='/patient' component={PatientHome} />
          <Route path='/prescriptions/patient/:id' component={MedicationPlanEdit} />
          <Route path='/addpresciption/patient/:id' component={AddPrescription} />
        </Switch>
      </Router>
    )
  }
}

export default App;

// class App extends Component {
//   state = {
//     isLoading: true,
//     groups: []
//   };

//   async componentDidMount() {
//     const response = await fetch('/caregivers');
//     const body = await response.json();
//     this.setState({ groups: body, isLoading: false });
//   }

//   render() {
//     const { groups, isLoading } = this.state;

//     if (isLoading) {
//       return <p>Loading...</p>;
//     }

//     return (
//       <div className="App">
//       <header className="App-header">
//         <img src={logo} className="App-logo" alt="logo" />
//         <div className="App-intro">
//           <h2>Caregivers</h2>
//           {groups.map(group => 
//             <div key={group.id}>
//               {group.name}
//             </div>
//             )}
//         </div>
//       </header>
//     </div>
//     );

//   }

// }

// export default App;

// import React from 'react';
// import logo from './logo.svg';
// import './App.css';

// function App() {
//   return (
//     <div className="App">
//       <header className="App-header">
//         <img src={logo} className="App-logo" alt="logo" />
//         <p>
//           Edit <code>src/App.js</code> and save to reload.
//         </p>
//         <a
//           className="App-link"
//           href="https://reactjs.org"
//           target="_blank"
//           rel="noopener noreferrer"
//         >
//           Learn React
//         </a>
//       </header>
//     </div>
//   );
// }

// export default App;
