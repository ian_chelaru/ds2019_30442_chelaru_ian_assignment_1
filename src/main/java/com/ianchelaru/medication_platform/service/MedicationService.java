package com.ianchelaru.medication_platform.service;

import com.ianchelaru.medication_platform.enitities.Medication;
import com.ianchelaru.medication_platform.errorhandler.ResourceNotFoundException;
import com.ianchelaru.medication_platform.repository.MedicationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MedicationService
{
    private final MedicationRepository medicationRepository;

    @Autowired
    public MedicationService(MedicationRepository medicationRepository)
    {
        this.medicationRepository = medicationRepository;
    }

    public Integer insert(Medication medication)
    {
        return medicationRepository.save(medication).getId();
    }

    public Medication findById(Integer id)
    {
        return medicationRepository.findById(id).
                orElseThrow(() -> new ResourceNotFoundException("Medication","id",id));
    }

    public List<Medication> findAll()
    {
        return medicationRepository.getAllOrderedByName();
    }

    public Integer update(Medication medication)
    {
        medicationRepository.findById(medication.getId()).orElseThrow(
                () -> new ResourceNotFoundException("Medication","id",medication.getId()));
        return medicationRepository.save(medication).getId();
    }

    public void delete(Medication medication)
    {
        medicationRepository.deleteById(medication.getId());
    }

    public Medication findByName(String name)
    {
        return medicationRepository.findByName(name)
                .orElseThrow(() -> new ResourceNotFoundException("Medication","name",name));
    }
}
