package com.ianchelaru.medication_platform.service;

import com.ianchelaru.medication_platform.enitities.Caregiver;
import com.ianchelaru.medication_platform.enitities.Patient;
import com.ianchelaru.medication_platform.errorhandler.ResourceNotFoundException;
import com.ianchelaru.medication_platform.repository.CaregiverRepository;
import com.ianchelaru.medication_platform.repository.PatientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PatientService
{
    private final PatientRepository patientRepository;
    private final CaregiverRepository caregiverRepository;

    @Autowired
    public PatientService(PatientRepository patientRepository, CaregiverRepository caregiverRepository) {
        this.patientRepository = patientRepository;
        this.caregiverRepository = caregiverRepository;
    }

    public Integer insert(Patient patient)
    {
        return patientRepository.save(patient).getId();
    }

    public Patient findById(Integer id)
    {
        return patientRepository.findById(id).
                orElseThrow(() -> new ResourceNotFoundException("Patient","id",id));
    }

    public List<Patient> findAll()
    {
        return patientRepository.findAll(Sort.by(Sort.Direction.ASC, "name"));
    }

    public Integer update(Patient patient)
    {
        patientRepository.findById(patient.getId()).orElseThrow(
                () -> new ResourceNotFoundException("Patient","id",patient.getId()));
        return patientRepository.save(patient).getId();
    }

    public void delete(Patient patient)
    {
        patientRepository.deleteById(patient.getId());
    }

    public List<Patient> findAllByCaregiverId(Integer id)
    {
        return patientRepository.findByCaregiver_id(id);
    }
}
