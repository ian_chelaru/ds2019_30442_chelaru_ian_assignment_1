package com.ianchelaru.medication_platform.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PrescriptionDTO {
    private String medicationName;
    private String dailyInterval;
    private Date startDate;
    private Date endDate;
    private Integer patientId;
}
