package com.ianchelaru.medication_platform.controller;

import com.ianchelaru.medication_platform.enitities.User;
import com.ianchelaru.medication_platform.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping(value = "/api/users")
public class UserController
{
    private final UserService userService;

    @Autowired
    public UserController(UserService userService)
    {
        this.userService = userService;
    }

    @GetMapping(value = "/{username}")
    public User findById(@PathVariable("username") String username)
    {
        return userService.findByUsername(username);
    }

    @PostMapping
    public Integer insert(@RequestBody User user)
    {
        return userService.insert(user);
    }
}
