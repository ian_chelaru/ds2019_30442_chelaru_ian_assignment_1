package com.ianchelaru.medication_platform.controller;

import com.ianchelaru.medication_platform.enitities.Caregiver;
import com.ianchelaru.medication_platform.service.CaregiverService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "/api/caregivers")
public class CaregiverController
{
    private final CaregiverService caregiverService;

    @Autowired
    public CaregiverController(CaregiverService caregiverService)
    {
        this.caregiverService = caregiverService;
    }

    @GetMapping(value = "/{id}")
    public Caregiver findById(@PathVariable("id") Integer id)
    {
        return caregiverService.findById(id);
    }

    @GetMapping
    public List<Caregiver> findAll()
    {
        return caregiverService.findAll();
    }

    @PostMapping
    public Integer insert(@RequestBody Caregiver caregiver)
    {
        return caregiverService.insert(caregiver);
    }

    //    @PostMapping()
    //    public Integer insertCaregiver(@ModelAttribute Caregiver caregiver)
    //    {
    //        return caregiverService.insertCaregiver(
    //                new Caregiver(caregiver.getName(),caregiver.getGender(),caregiver.getAddress()));
    //    }

    @PutMapping
    public Integer update(@RequestBody Caregiver caregiver)
    {
        return caregiverService.update(caregiver);
    }

    @DeleteMapping
    public void delete(@RequestBody Caregiver caregiver)
    {
        caregiverService.delete(caregiver);
    }

}
