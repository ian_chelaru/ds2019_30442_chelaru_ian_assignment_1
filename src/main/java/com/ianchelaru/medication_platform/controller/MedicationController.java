package com.ianchelaru.medication_platform.controller;

import com.ianchelaru.medication_platform.enitities.Medication;
import com.ianchelaru.medication_platform.service.MedicationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "/api/medications")
public class MedicationController
{
    private final MedicationService medicationService;

    @Autowired
    public MedicationController(MedicationService medicationService)
    {
        this.medicationService = medicationService;
    }

    @GetMapping(value = "/{id}")
    public Medication findById(@PathVariable("id") Integer id)
    {
        return medicationService.findById(id);
    }

    @GetMapping
    public List<Medication> findAll()
    {
        return medicationService.findAll();
    }

    @PostMapping
    public Integer insert(@RequestBody Medication medication)
    {
        return medicationService.insert(medication);
    }

    @PutMapping
    public Integer update(@RequestBody Medication medication)
    {
        return medicationService.update(medication);
    }

    @DeleteMapping
    public void delete(@RequestBody Medication medication)
    {
        medicationService.delete(medication);
    }

    @GetMapping(value="/name/{name}")
    public Medication findByName(@PathVariable("name") String name)
    {
        return medicationService.findByName(name);
    }
}
