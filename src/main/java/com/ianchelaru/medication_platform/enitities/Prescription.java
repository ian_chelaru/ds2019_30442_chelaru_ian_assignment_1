package com.ianchelaru.medication_platform.enitities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Prescription
{
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(unique = true, nullable = false)
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "medication_id", nullable = false)
    private Medication medication;

    @Column(nullable = false)
    private String dailyInterval;

    @Column(nullable = false)
    private Date startDate;

    @Column(nullable = false)
    private Date endDate;

    @ManyToOne
    @JoinColumn(name = "patient_id", nullable = false)
    @JsonIgnore
    private Patient patient;

    public Prescription(Medication medication, String dailyInterval, Date startDate, Date endDate, Patient patient) {
        this.medication = medication;
        this.dailyInterval = dailyInterval;
        this.startDate = startDate;
        this.endDate = endDate;
        this.patient = patient;
    }

    //    @ManyToOne
//    @JoinColumn(name = "medication_plan_id", nullable = false)
//    private MedicationPlan medicationPlan;
}
